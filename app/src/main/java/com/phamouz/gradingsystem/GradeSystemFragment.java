package com.phamouz.gradingsystem;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class GradeSystemFragment extends Fragment {



    public GradeSystemFragment() {
        // Required empty public constructor
    }






    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {



        final View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.fragment_grade_system, null);

        final TextView gradeA;
        final ImageButton InfoA;
        final TextView gradeB;
        final ImageButton InfoB;


        gradeA = (TextView) view.findViewById(R.id.gradeA);
        gradeA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(view.getContext(), "Grading System A is Selected", Toast.LENGTH_SHORT).show();

            }
        });

        InfoA = (ImageButton)view.findViewById(R.id.infoA);
        InfoA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".InfoAActivity");
                startActivity(intent);
            }
        });

        gradeB = (TextView)view.findViewById(R.id.gradeB);
        gradeB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(view.getContext(), "Grading System B is Selected", Toast.LENGTH_SHORT).show();
            }
        });

        InfoB = (ImageButton)view.findViewById(R.id.InfoB);
        InfoB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(".InfoBActivity");
                startActivity(intent);
            }
        });
        return view;
    }


}
