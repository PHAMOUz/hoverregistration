package com.phamouz.gradingsystem;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class GradeMeFragment extends Fragment {


    public GradeMeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_grade_me, container, false);

        final EditText Score;
        final Button finalGrade;

        Score = (EditText)view.findViewById(R.id.score);
        finalGrade = (Button)view.findViewById(R.id.finalGrade);
        finalGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String un = Score.getText().toString();
                if (un.isEmpty()) {

                    Toast.makeText(view.getContext(), "Please Enter A Score", Toast.LENGTH_SHORT).show();

                } else if (un.equals("100")) {
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("99")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("98")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("96")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("95")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("94")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("93")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("92")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("91")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("90")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("89")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("88")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("87")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("86")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("85")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("84")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("83")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("82")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("81")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("80")){
                    Toast.makeText(view.getContext(), "Excellent", Toast.LENGTH_SHORT).show();
                }else if(un.equals("79")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("78")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("77")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("76")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("75")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("74")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("73")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("72")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("71")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("70")){
                    Toast.makeText(view.getContext(), "Very Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("69")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("68")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("67")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("66")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("65")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("64")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("63")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("62")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("61")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("60")){
                    Toast.makeText(view.getContext(), "Good", Toast.LENGTH_SHORT).show();
                }else if(un.equals("59")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("58")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("57")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("56")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("55")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("54")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("53")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("52")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("51")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("50")){
                    Toast.makeText(view.getContext(), "Average", Toast.LENGTH_SHORT).show();
                }else if(un.equals("49")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("48")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("47")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("46")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("45")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("44")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("43")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("42")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("41")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("40")){
                    Toast.makeText(view.getContext(), "Weak", Toast.LENGTH_SHORT).show();
                }else if(un.equals("39")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("38")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("37")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("36")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("35")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("33")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("32")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("31")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("30")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("29")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("28")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("27")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("26")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("25")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("24")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("23")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("22")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("21")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("20")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("19")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("18")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("17")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("16")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("15")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("14")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("13")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("12")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("11")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("10")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("9")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("8")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("7")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("6")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("5")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("4")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("3")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("2")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("1")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else if(un.equals("0")){
                    Toast.makeText(view.getContext(), "Failed", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(view.getContext(), "Please Enter a Valid Score", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

}
